echo "Please input your device name"
read device
_defconfig=_defconfig
KERNEL_SOURCE_DIR=/data/sdm845
CLANG_DIR=/data/clang
GCC32_DIR=/data/arm-linux-androideabi-4.9
GCC64_DIR=/data/aarch64-linux-android-4.9
args="O=../out \
	ARCH=arm64 \
	HOSTCC=${CLANG_DIR}/bin/clang \
	HOSTCXX=${CLANG_DIR}/bin/clang++ \
	CC=${CLANG_DIR}/bin/clang \
	CLANG_TRIPLE=aarch64-linux-gnu- \
	CROSS_COMPILE=${GCC64_DIR}/bin/aarch64-linux-android- \
	CROSS_COMPILE_ARM32=${GCC32_DIR}/bin/arm-linux-androideabi- \
	LD=${CLANG_DIR}/bin/ld.lld \
	OBJCOPY=${CLANG_DIR}/bin/llvm-objcopy \
	OBJDUMP=${CLANG_DIR}/bin/llvm-objdump \
	STRIP=${CLANG_DIR}/bin/llvm-strip \
	NM=${CLANG_DIR}/bin/llvm-nm \
	AS=${CLANG_DIR}/bin/llvm-as \
	AR=${CLANG_DIR}/bin/llvm-ar"
cd ${KERNEL_SOURCE_DIR}
export PATH=/data/clang/bin:${PATH}
make ${args} $device$_defconfig
make -j$(nproc --all) ${args}
